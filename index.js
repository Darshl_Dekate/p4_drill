const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


//1. Get all items that are available 
function allItem(elements){

    return elements;
}

const result = items.map((x)=> x)
console.log(result)



    //2. Get all items containing only Vitamin C.
function vitC(items){

    if(items.contains=='Vitamin C'){
        return items
    }


}
const result1= items.filter(vitC)
console.log(result1)


   // 3. Get all items containing Vitamin A.

const output  =items.filter((each)=>{
    if(each.contains.includes("Vitamin A"))
    {
        return each;
    }
})
console.log(output)



//     4. Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": [
    
//             "Vitamin K": ["Mango"],
// // //         }
function vitamin(acc,curr){
        const arr = curr.contains.split(",");
        for(let i in arr)
        {
            let key = arr[i].trim();

            if(acc[key]){
                acc[key].push(curr["name"])               
            }
            else{
    
                acc[key]=[]
                acc[key].push(curr["name"])
            }
        }   
        return acc
    }
    
    const output1= items.reduce(vitamin,[])
    
    console.log(output1)


//         and so on for all items and all Vitamins.
//     5. Sort items based on number of Vitamins they contain.

// function vitamin(acc,curr){
//     if(acc[curr["name"]]){
//         acc[curr["name"]].push(curr["contains"])
        
//     }
//     else{
//         acc[curr["name"]]=[]
//         acc[curr["name"]].push(curr["contains"])
//     }
//     return acc

// }

// const output2= items.reduce(vitamin,[])

// console.log(output2)


function sortVitamin(a, b){
{
    if(a["contains"].length < b["contains"].length)
    {
        return -1;
    }
    else if(a["contains"].length < b["contains"].length)
    {
        return 1;
    }
    return 0;
}
}
const output3 = items.sort(sortVitamin)
console.log(output3);